package com.speks.trail.app;

import android.app.Application;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.messaging.FirebaseMessaging;
import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.app.dagger.AppModule;
import com.speks.trail.app.dagger.DaggerAppComponent;
import com.speks.trail.helpers.Constants;

import static android.content.ContentValues.TAG;

public class Trail extends Application {
    AppComponent appComponent;

    public static AppComponent getAppComponent(AppCompatActivity activity) {
        return ((Trail) activity.getApplication()).appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.NOTIFICATION_TOPIC)
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful())
                        Log.d(TAG, "onCreate: could not subscribed to violations");
                    else
                        Log.d(TAG, "onCreate: subscribed to violations");

                });
    }
}
