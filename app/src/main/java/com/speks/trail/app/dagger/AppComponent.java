package com.speks.trail.app.dagger;


import android.content.Context;

import com.speks.trail.network.APINetwork;
import com.speks.trail.network.NetworkModule;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.storage.StorageModule;
import com.squareup.picasso.Picasso;

import dagger.Component;

@Component(modules = {AppModule.class, NetworkModule.class, StorageModule.class})
@AppScope
public interface AppComponent {
    Context context();

    PreferencesManager preferenceManager();

    APINetwork apiNetwork();

    Picasso picasso();

}
