package com.speks.trail.app.dagger;

import javax.inject.Scope;

@Scope
public @interface AppScope {
}
