package com.speks.trail.helpers;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;

public class BaseModel {
    private APINetwork apiNetwork;
    private PreferencesManager preferencesManager;

    public BaseModel(APINetwork apiNetwork, PreferencesManager preferencesManager) {
        this.apiNetwork = apiNetwork;
        this.preferencesManager = preferencesManager;
    }

    public void savePreference(String key, String value) {
        preferencesManager.save(key, value);
    }

    public void detetePreference(String key) {
        preferencesManager.save(key, null);
    }

    public String getPreference(String key) {
        return preferencesManager.get(key);
    }

    protected APINetwork getApiNetwork() {
        return apiNetwork;
    }

    protected PreferencesManager getPreferencesManager() {
        return preferencesManager;
    }
}
