package com.speks.trail.helpers;

import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public abstract class BaseView extends ConstraintLayout {
    private LoadingDialog loadingDialog;

    public BaseView(Context context) {
        super(context);
    }

    public void showLoading() {
        loadingDialog = new LoadingDialog(getActivity());
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    public void hideLoading() {
        if (loadingDialog == null) return;
        loadingDialog.dismiss();
        loadingDialog = null;
    }

    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String message, int duration) {
        Toast.makeText(getActivity(), message, duration).show();
    }

    public void showMessage(String msg) {
        Snackbar.make(getActivity().findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
                .show();
    }

    public void showNetworkError(Throwable e) {
        if (e instanceof TimeoutException)
            showMessage("Error!, Connection Time Out");
        else if (e instanceof IOException)
            showMessage("Error!, Please check your network connection");
        else
            showMessage("Error! Something went wrong");
    }

    protected abstract AppCompatActivity getActivity();
}
