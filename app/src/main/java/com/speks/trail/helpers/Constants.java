package com.speks.trail.helpers;

public class Constants {

    //cache
    public static final String HTTP_DIR_CACHE = "vep";
    public static final int CACHE_SIZE = 10 * 1024 * 1024;


    //    notification
    public static final String channelId = "channel-01";
    public static final String NOTIFICATION_TOPIC = "violations";
    public static final String channelName = "Violation Events";
    public static final String IS_DETECTION = "detection";

    //preferences key
    public static class PKEYS {
        public static final String PREFERENCE_NAME = "selfie_sh";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String REFRESH_TOKEN = "refresh_token";
        public static final String ACCESS_TOKEN_EXPIRATION = "access_token_expiration";
        public static final String USER_NAME = "user_name";
    }


    //preferences key
    public static class API_FAKE {
        public static final String BASE_URL = " http://www.mocky.io/v2/";
        public static final String LOGIN = "5d2e0de52e00006200c582cf";
        public static final String DASHBOARD = "5d1b8a6134000093c6000887";
        public static final String RECENT = "5d309b8d320000028b204636";
    }

    //preferences key
    public static class API {
        public static final String BASE_URL = "https://djangoappp.herokuapp.com/";
        public static final String LOGIN = "api/token/";
        public static final String DASHBOARD = "5d1b8a6134000093c6000887";
        public static final String RECENT = "5d1b8a6134000093c6000887";

    }
}
