package com.speks.trail.helpers;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;

import com.speks.trail.R;

public class LoadingDialog extends Dialog {
    public LoadingDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.custom_dialog);
        findViewById(R.id.iv_s).startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rotate));
        findViewById(R.id.iv_b).startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.rotate_reverse));
    }
}
