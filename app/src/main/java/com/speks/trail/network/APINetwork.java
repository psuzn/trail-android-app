package com.speks.trail.network;

import com.speks.trail.helpers.Constants;
import com.speks.trail.network.models.dashboard.DashboardResponse;
import com.speks.trail.network.models.login.LoginResponse;
import com.speks.trail.network.models.recent.RecentResponse;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface APINetwork {

    @POST(Constants.API.LOGIN)
    @FormUrlEncoded
    Observable<LoginResponse> login(@FieldMap HashMap<String, String> loginParams);

    @POST()
    @FormUrlEncoded
    Observable<LoginResponse> login(@Url() String url, @FieldMap HashMap<String, String> loginParams);

    @POST()
    @FormUrlEncoded
    Observable<DashboardResponse> dashboard(@Url() String url, @FieldMap HashMap<String, String> params);


    @POST()
    @FormUrlEncoded
    Observable<RecentResponse> recent(@Url() String url, @FieldMap HashMap<String, String> params);
}
