package com.speks.trail.network;


import android.content.Context;
import android.util.Log;

import com.speks.trail.BuildConfig;
import com.speks.trail.app.dagger.AppScope;
import com.speks.trail.helpers.Constants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    @AppScope
    @Provides
    Cache cache(Context context) {
        return new Cache(new File(context.getCacheDir(), Constants.HTTP_DIR_CACHE), Constants.CACHE_SIZE);

    }

    @AppScope
    @Provides
    OkHttpClient provideClient(Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    request = request.newBuilder()
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .build();
                    return chain.proceed(request);
                })
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .cache(cache);

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new LoggingInterceptor());
        }
        return builder.build();
    }

    @AppScope
    @Provides
    Retrofit retrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder().client(okHttpClient)
                .baseUrl(Constants.API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @AppScope
    @Provides
    APINetwork apiNetwork(Retrofit retrofit) {
        return retrofit.create(APINetwork.class);
    }

    @AppScope
    @Provides
    Picasso picasso(Context context, OkHttpClient okHttpClient) {
        return new Picasso.Builder(context)
//                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();
    }

    class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Log.d("OkHttp", String.format("--> Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));

            Buffer requestBuffer = new Buffer();
            request.body().writeTo(requestBuffer);
            Log.d("OkHttp", requestBuffer.readUtf8());

            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Log.d("OkHttp", String.format("<-- Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            MediaType contentType = response.body().contentType();
            String content = response.body().string();
            Log.d("OkHttp", content);

            ResponseBody wrappedBody = ResponseBody.create(contentType, content);
            return response.newBuilder().body(wrappedBody).build();
        }
    }
}
