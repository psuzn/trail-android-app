package com.speks.trail.network.models.dashboard;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DashboardItem {

	@SerializedName("reports")
	private int reports;

	@SerializedName("cctv")
	private String cctv;

	public void setReports(int reports){
		this.reports = reports;
	}

	public int getReports(){
		return reports;
	}

	public void setCctv(String cctv){
		this.cctv = cctv;
	}

	public String getCctv(){
		return cctv;
	}
}