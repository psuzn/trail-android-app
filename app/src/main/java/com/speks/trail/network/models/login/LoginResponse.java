package com.speks.trail.network.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    private final static long serialVersionUID = 7437798848296888830L;
    @SerializedName("access")
    @Expose
    private String token;
    @SerializedName("refresh")
    @Expose
    private String refresh;
    @SerializedName("status")
    @Expose
    private Long status;

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

}