package com.speks.trail.network.models.recent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventItem implements Serializable {

    private final static long serialVersionUID = 5604927531419823753L;
    @SerializedName("cctv")
    @Expose
    private String cctv;
    @SerializedName("time")
    @Expose
    private Long time;
    @SerializedName("photos")
    @Expose
    private List<String> photos = null;
    @SerializedName("video")
    @Expose
    private String video;

    public String getCctv() {
        return cctv;
    }

    public void setCctv(String cctv) {
        this.cctv = cctv;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

}