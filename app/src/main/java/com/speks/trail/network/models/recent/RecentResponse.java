package com.speks.trail.network.models.recent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RecentResponse implements Serializable {

    private final static long serialVersionUID = -3800322061711484072L;
    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("data")
    @Expose
    private List<EventItem> data = null;

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public List<EventItem> getEvents() {
        return data;
    }

    public void setEvents(List<EventItem> data) {
        this.data = data;
    }

}