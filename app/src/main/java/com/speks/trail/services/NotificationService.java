package com.speks.trail.services;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.speks.trail.helpers.Constants;
import com.speks.trail.helpers.Utils;
import com.speks.trail.ui.activities.home.HomeActivity;

import static android.content.ContentValues.TAG;

public class NotificationService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "onMessageReceived: " +  remoteMessage.getData().get("message"));
        showNotification();
    }

    private void showNotification() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(Constants.IS_DETECTION, true);
        Utils.showNotification(getApplicationContext(), "Alert!!", "CCTV from Maitighar detected a rule violation.", intent);
    }
}
