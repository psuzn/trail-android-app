package com.speks.trail.storage;


import android.content.Context;

import com.speks.trail.app.dagger.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @AppScope
    PreferencesManager preferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
