package com.speks.trail.ui.activities.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.speks.trail.R;
import com.speks.trail.app.Trail;
import com.speks.trail.helpers.Constants;
import com.speks.trail.helpers.Utils;
import com.speks.trail.ui.activities.home.dagger.DaggerHomeComponent;
import com.speks.trail.ui.activities.home.dagger.HomeModule;
import com.speks.trail.ui.activities.home.mv.HomeModel;
import com.speks.trail.ui.activities.home.mv.HomeView;
import com.speks.trail.ui.activities.login.LoginActivity;

import javax.inject.Inject;

public class HomeActivity extends AppCompatActivity {

    @Inject
    HomeModel model;
    @Inject
    HomeView view;

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerHomeComponent.builder()
                .appComponent(Trail.getAppComponent(this))
                .homeModule(new HomeModule(this))
                .build()
                .inject(this);


        setContentView(view);
        ((TextView) view.binding.navView.getHeaderView(0).findViewById(R.id.nav_view_text))
                .setText(model.getPreference(Constants.PKEYS.USER_NAME));
        setupDrawer();
        Utils.setupStatusBar(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    public boolean logout() {
        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setPositiveButton("Yes", (dialogInterface, i) -> {
                    model.detetePreference(Constants.PKEYS.USER_NAME);
                    model.detetePreference(Constants.PKEYS.ACCESS_TOKEN);
                    view.showLoading();
                    new Handler().postDelayed(() -> {
                        view.hideLoading();
                        view.showToast("Logged out");
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    }, 600);

                })
                .setNegativeButton("No", null)
                .show();
        view.binding.drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    void setupDrawer() {
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_dashboard, R.id.nav_recent, R.id.nav_history)
                .setDrawerLayout(view.binding.drawerLayout)
                .build();
        setSupportActionBar(findViewById(R.id.toolbar));
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(view.binding.navView, navController);
        view.binding.navView.getMenu().findItem(R.id.nav_logout).setOnMenuItemClickListener(item -> this.logout());

        if (getIntent().getBooleanExtra(Constants.IS_DETECTION, false)) {
            navController.navigate(R.id.nav_recent);
        }
    }
}
