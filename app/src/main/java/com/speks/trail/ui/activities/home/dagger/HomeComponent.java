package com.speks.trail.ui.activities.home.dagger;



import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.ui.activities.home.HomeActivity;

import dagger.Component;

@HomeScope
@Component(modules = HomeModule.class, dependencies = {AppComponent.class})
public interface HomeComponent {
    void inject(HomeActivity activity);
}
