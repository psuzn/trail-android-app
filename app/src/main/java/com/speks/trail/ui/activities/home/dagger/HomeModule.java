package com.speks.trail.ui.activities.home.dagger;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.ui.activities.home.mv.HomeModel;
import com.speks.trail.ui.activities.home.mv.HomeView;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {
    private AppCompatActivity activity;

    public HomeModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @HomeScope
    HomeModel model(APINetwork appNetwork, PreferencesManager preferencesManager) {
        return new HomeModel(appNetwork, preferencesManager);
    }

    @Provides
    @HomeScope
    HomeView view() {
        return new HomeView(activity);
    }
}
