package com.speks.trail.ui.activities.home.mv;

import com.speks.trail.helpers.BaseModel;
import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;

public class HomeModel extends BaseModel {
    public HomeModel(APINetwork apiNetwork, PreferencesManager preferencesManager) {
        super(apiNetwork, preferencesManager);
    }
}
