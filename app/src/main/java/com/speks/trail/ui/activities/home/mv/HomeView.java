package com.speks.trail.ui.activities.home.mv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.speks.trail.R;
import com.speks.trail.databinding.ActivityHomeBinding;
import com.speks.trail.helpers.BaseView;

public class HomeView extends BaseView {

    public ActivityHomeBinding binding;
    AppCompatActivity activity;

    public HomeView(AppCompatActivity activity) {
        super(activity);
        this.activity = activity;
        binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.activity_home, this, true);
    }

    @Override
    protected AppCompatActivity getActivity() {
        return activity;
    }
}
