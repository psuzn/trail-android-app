package com.speks.trail.ui.activities.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.app.Trail;
import com.speks.trail.helpers.Constants;
import com.speks.trail.helpers.Utils;
import com.speks.trail.network.models.login.LoginResponse;
import com.speks.trail.ui.activities.home.HomeActivity;
import com.speks.trail.ui.activities.login.dagger.DaggerLoginComponent;
import com.speks.trail.ui.activities.login.dagger.LoginModule;
import com.speks.trail.ui.activities.login.mv.LoginModel;
import com.speks.trail.ui.activities.login.mv.LoginView;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {

    @Inject
    LoginView view;

    @Inject
    LoginModel model;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerLoginComponent.builder()
                .appComponent(Trail.getAppComponent(this))
                .loginModule(new LoginModule(this))
                .build().inject(this);
        setContentView(view);
        Utils.setupStatusBar(this);

        if (model.loggedIn()) {
            onLoggedIn();
            return;
        }

        view.binding.btnShowRegister.setOnClickListener(v -> {
            view.binding.vViewPager.setCurrentItem(1, true);
            view.binding.tvLoginSignUp.setText("Register");
        });
        view.binding.btnShowLoginUp.setOnClickListener(v -> {
            view.binding.vViewPager.setCurrentItem(0, true);
            view.binding.tvLoginSignUp.setText("Log In");
        });

        view.binding.btnLogin.setOnClickListener(v -> {
            String userName = view.binding.etLoginEmail.getText().toString();
            String password = view.binding.etLoginPassword.getText().toString();
            String errorMsg = validateLoginData(userName, password);
            if (errorMsg == null) {
                login(userName, password);
            } else
                view.showMessage(errorMsg);
        });

        view.binding.btnRegister.setOnClickListener(v -> {

            String email = view.binding.etRegisterEmail.getText().toString();
            String password = view.binding.etRegisterPassword.getText().toString();
            String passwordVerify = view.binding.etRegisterPasswordVerify.getText().toString();

            String errorMsg = validateRegisterData(email, password, passwordVerify);
            if (errorMsg != null) {
                register(email, password);
            } else
                view.showMessage(errorMsg);
        });


    }

    private void register(String email, String password) {

    }


    void login(String username, String password) {
        HashMap<String, String> loginParams = new HashMap<>();
        loginParams.put("username", username);
        loginParams.put("password", password);
        view.showLoading();
        DisposableObserver<LoginResponse> disposableObserver =
                new DisposableObserver<LoginResponse>() {
                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        model.savePreference(Constants.PKEYS.ACCESS_TOKEN, loginResponse.getToken());
                        model.savePreference(Constants.PKEYS.USER_NAME, username);
                        view.hideLoading();
                        onLoggedIn();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideLoading();
                        view.showNetworkError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                };
        model.getLoginDisposable(loginParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }

    private void onLoggedIn() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
    }

    private String validateLoginData(String username, String password) {
        if (username.length() == 0)
            return "Username is empty";

//        if (!Utils.isValidEmailId(username))
//            return "Email is not valid";

        if (password.length() == 0)
            return "Password is empty";
        return null;

    }

    private String validateRegisterData(String email, String password, String password1) {
        if (email.length() == 0)
            return "Username is empty";

//        if (!Utils.isValidEmailId(email))
//            return "Email is not valid";

        if (!password.equals(password1))
            return "Passwords are not same";

        if (password.length() == 0)
            return "Password is empty";

        if (password.length() < 5)
            return "password must be at least 5 characters long";

        return null;

    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
