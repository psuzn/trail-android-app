package com.speks.trail.ui.activities.login.dagger;



import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.ui.activities.login.LoginActivity;

import dagger.Component;

@LoginScope
@Component(modules = LoginModule.class, dependencies = {AppComponent.class})
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
