package com.speks.trail.ui.activities.login.dagger;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.ui.activities.login.mv.LoginModel;
import com.speks.trail.ui.activities.login.mv.LoginView;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {
    private AppCompatActivity activity;

    public LoginModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @LoginScope
    LoginModel loginModel(APINetwork appNetwork, PreferencesManager preferencesManager) {
        return new LoginModel(appNetwork, preferencesManager);
    }

    @Provides
    @LoginScope
    LoginView loginView() {
        return new LoginView(activity);
    }
}
