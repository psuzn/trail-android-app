package com.speks.trail.ui.activities.login.dagger;

import javax.inject.Scope;

@Scope
@interface LoginScope {
}
