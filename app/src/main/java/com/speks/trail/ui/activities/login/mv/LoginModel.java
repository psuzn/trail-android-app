package com.speks.trail.ui.activities.login.mv;


import com.speks.trail.helpers.BaseModel;
import com.speks.trail.helpers.Constants;
import com.speks.trail.network.APINetwork;
import com.speks.trail.network.models.login.LoginResponse;
import com.speks.trail.storage.PreferencesManager;

import java.util.HashMap;

import io.reactivex.Observable;

public class LoginModel extends BaseModel {

    public LoginModel(APINetwork apiNetwork, PreferencesManager preferencesManager) {
        super(apiNetwork, preferencesManager);

    }

    public Observable<LoginResponse> getLoginDisposable(HashMap<String, String> loginParams) {
//        return getApiNetwork().login(loginParams);
        return getApiNetwork().login(Constants.API_FAKE.BASE_URL+Constants.API_FAKE.LOGIN,loginParams);

    }

//    Observable<RegisterResponse> getRegisterDisposable(HashMap<String, String> registerParams) {
//        return appNetwork.register(registerParams);
//    }

    public boolean loggedIn() {
        return getPreference(Constants.PKEYS.ACCESS_TOKEN) != null;
    }
}
