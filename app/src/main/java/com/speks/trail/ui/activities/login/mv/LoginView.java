package com.speks.trail.ui.activities.login.mv;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.speks.trail.R;
import com.speks.trail.databinding.ActivityLoginBinding;
import com.speks.trail.helpers.BaseView;

public class LoginView extends BaseView {
    AppCompatActivity activity;
    public ActivityLoginBinding binding;

    @SuppressLint("ClickableViewAccessibility")
    public LoginView(AppCompatActivity activity) {
        super(activity);
        this.activity = activity;
        binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.activity_login, this, true);
        binding.vViewPager.setAdapter(new VerticalViewPagerAdaptor());
        binding.vViewPager.setOnTouchListener((v1, event) -> true);
    }

    @Override
    protected AppCompatActivity getActivity() {
        return activity;
    }

    class VerticalViewPagerAdaptor extends PagerAdapter {
        @Override
        public int getCount() {
            return 2;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            return position == 0 ? findViewById(R.id.login_container) : findViewById(R.id.sign_up_container);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }
    }
}
