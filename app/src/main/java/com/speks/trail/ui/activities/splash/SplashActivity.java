package com.speks.trail.ui.activities.splash;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.R;
import com.speks.trail.app.Trail;
import com.speks.trail.ui.activities.login.LoginActivity;
import com.speks.trail.ui.activities.home.HomeActivity;
import com.speks.trail.ui.activities.splash.dagger.DaggerSplashComponent;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity {

    @Inject
    SplashModel model;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        DaggerSplashComponent.builder()
                .appComponent(Trail.getAppComponent(this))
                .build()
                .inject(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(Color.WHITE);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        new Handler().postDelayed(() -> {
            Intent i;
            if (model.loggedIn())
                i = new Intent(SplashActivity.this, HomeActivity.class);
            else
                i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }, 2000);
    }
}