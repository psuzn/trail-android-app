package com.speks.trail.ui.activities.splash;

import com.speks.trail.helpers.BaseModel;
import com.speks.trail.helpers.Constants;
import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;

public class SplashModel extends BaseModel {
    public SplashModel(APINetwork apiNetwork, PreferencesManager preferencesManager) {
        super(apiNetwork, preferencesManager);
    }

    boolean loggedIn() {
        return getPreference(Constants.PKEYS.ACCESS_TOKEN) != null;
    }
}
