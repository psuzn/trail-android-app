package com.speks.trail.ui.activities.splash.dagger;


import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.ui.activities.splash.SplashActivity;

import dagger.Component;

@SplashScope
@Component(modules = SplashModule.class, dependencies = {AppComponent.class})
public interface SplashComponent {
    void inject(SplashActivity activity);
}
