package com.speks.trail.ui.activities.splash.dagger;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.ui.activities.splash.SplashModel;

import dagger.Module;
import dagger.Provides;

@Module
class SplashModule {

    @Provides
    @SplashScope
    SplashModel model(APINetwork appNetwork, PreferencesManager preferencesManager) {
        return new SplashModel(appNetwork, preferencesManager);
    }

}
