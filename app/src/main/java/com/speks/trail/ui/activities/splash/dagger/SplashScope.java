package com.speks.trail.ui.activities.splash.dagger;

import javax.inject.Scope;

@Scope
@interface SplashScope {
}
