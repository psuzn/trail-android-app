package com.speks.trail.ui.adaptor;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.speks.trail.R;
import com.speks.trail.databinding.EventListItemBinding;
import com.speks.trail.network.models.recent.EventItem;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListAdaptor extends RecyclerView.Adapter<ListAdaptor.ItemHolder> {
    private List<EventItem> eventItems = new ArrayList<>();
    private AppCompatActivity activity;
    private Picasso picasso;

    public ListAdaptor(AppCompatActivity activity, Picasso picasso) {
        this.activity = activity;
        this.picasso = picasso;
    }


    public void setData(List<EventItem> eventItems) {
        this.eventItems.clear();
        this.eventItems.addAll(eventItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemHolder(DataBindingUtil.inflate(LayoutInflater.from(activity),
                R.layout.event_list_item, parent, false));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        EventItem eventItem = eventItems.get(position);
        holder.binding.vpPhotos.setAdapter(new ViewPagerAdaptor(activity, picasso, eventItem));
        holder.binding.tvCctv.setText(eventItem.getCctv());
        holder.binding.tvDate.setText(SimpleDateFormat.getDateTimeInstance().format(new Date(eventItem.getTime())));
        holder.binding.indicator.setViewPager(holder.binding.vpPhotos);
        if (position == eventItems.size() - 1) {
            ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(holder.itemView.getRootView().getLayoutParams());
            marginLayoutParams.setMargins(0, 0, 0, 0);
            holder.binding.getRoot().setLayoutParams(new RecyclerView.LayoutParams(marginLayoutParams));
        }
    }

    @Override
    public int getItemCount() {
        return eventItems.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder {
        public EventListItemBinding binding;

        ItemHolder(@NonNull EventListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
