package com.speks.trail.ui.adaptor;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.speks.trail.R;
import com.speks.trail.databinding.FullscreenViewPagerItemImageBinding;
import com.speks.trail.databinding.ViewPagerItemImageBinding;
import com.speks.trail.ui.views.ImagePreviewDialog;
import com.speks.trail.network.models.recent.EventItem;
import com.squareup.picasso.Picasso;

public class ViewPagerAdaptor extends PagerAdapter {

    private Picasso picasso;
    private AppCompatActivity activity;
    private EventItem eventItem;
    private View.OnClickListener onClickListener;

    public ViewPagerAdaptor(AppCompatActivity activity, Picasso picasso, EventItem eventItem) {
        this.activity = activity;
        this.picasso = picasso;
        this.eventItem = eventItem;
    }

    public ViewPagerAdaptor(AppCompatActivity activity, Picasso picasso, EventItem eventItem, View.OnClickListener onClickListener) {
        this.activity = activity;
        this.picasso = picasso;
        this.eventItem = eventItem;
        this.onClickListener = onClickListener;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        if(onClickListener!=null){
            FullscreenViewPagerItemImageBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.fullscreen_view_pager_item_image, collection, true);
            picasso.load(eventItem.getPhotos().get(position)).into(binding.ivImage);
            binding.ivImage.setOnClickListener(onClickListener);
            return binding.getRoot();
        }
        else{
            ViewPagerItemImageBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.view_pager_item_image, collection, true);
            picasso.load(eventItem.getPhotos().get(position)).fit().into(binding.ivImage);
            binding.ivImage.setOnClickListener(onClickListener);
            binding.ivImage.setOnClickListener(view -> new ImagePreviewDialog(activity, eventItem, 0, picasso).show());
            return binding.getRoot();
        }

    }

    @Override
    public void destroyItem(ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return eventItem.getPhotos().size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
