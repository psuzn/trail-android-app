package com.speks.trail.ui.fragments.dashboard;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.speks.trail.R;
import com.speks.trail.app.Trail;
import com.speks.trail.databinding.TableRowBinding;
import com.speks.trail.helpers.Constants;
import com.speks.trail.helpers.Utils;
import com.speks.trail.network.models.dashboard.DashboardItem;
import com.speks.trail.network.models.dashboard.DashboardResponse;
import com.speks.trail.ui.activities.home.HomeActivity;
import com.speks.trail.ui.fragments.dashboard.dagger.DaggerDashboardComponent;
import com.speks.trail.ui.fragments.dashboard.dagger.DashboardModule;
import com.speks.trail.ui.fragments.dashboard.mv.DashboardModel;
import com.speks.trail.ui.fragments.dashboard.mv.DashboardView;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class DashboardFragment extends Fragment {

    @Inject
    DashboardModel model;
    @Inject
    DashboardView view;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DaggerDashboardComponent.builder()
                .appComponent(Trail.getAppComponent((AppCompatActivity) getActivity()))
                .dashboardModule(new DashboardModule((AppCompatActivity) getActivity()))
                .build()
                .inject(this);
        getData(false);
        view.binding.srRefreshLayout.setOnRefreshListener(() -> getData(true));
        return view;
    }


    private void getData(boolean refresh) {
        HashMap<String, String> loginParams = new HashMap<>();
        loginParams.put("token", model.getPreference(Constants.PKEYS.ACCESS_TOKEN));
        if (!refresh) view.showLoading();
        DisposableObserver<DashboardResponse> disposableObserver =
                new DisposableObserver<DashboardResponse>() {
                    @Override
                    public void onNext(DashboardResponse response) {
                        view.binding.tlTable.removeViews(2, view.binding.tlTable.getChildCount() - 3);

                        populateTable(response.getDashboardItems());
                        view.binding.srRefreshLayout.setRefreshing(false);
                        view.hideLoading();
                        if (refresh) view.showToast("Refreshed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        view.hideLoading();
                        view.binding.srRefreshLayout.setRefreshing(false);
                        view.showNetworkError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                };
        model.getDashboardDisposable(loginParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }

    private void populateTable(List<DashboardItem> items) {
        for (DashboardItem item : items) {
            TableRowBinding binding =
                    DataBindingUtil.inflate(getLayoutInflater(), R.layout.table_row, view.binding.tlTable, false);
            binding.tvSn.setText(String.valueOf(view.binding.tlTable.getChildCount() - 2));
            binding.tvCctv.setText(item.getCctv());
            binding.tvReports.setText(String.valueOf(item.getReports()));
            if (view.binding.tlTable.getChildCount() % 2 == 0)
                binding.getRoot().setBackground(
                        new ColorDrawable(ContextCompat.getColor(getContext(), R.color.colorPrimaryTransparent)));
            view.binding.tlTable.addView(binding.getRoot(), view.binding.tlTable.getChildCount() - 1);
        }
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}