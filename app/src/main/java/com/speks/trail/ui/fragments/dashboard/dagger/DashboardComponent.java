package com.speks.trail.ui.fragments.dashboard.dagger;


import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.ui.activities.splash.SplashActivity;
import com.speks.trail.ui.fragments.dashboard.DashboardFragment;

import dagger.Component;

@DashboardScope
@Component(modules = DashboardModule.class, dependencies = {AppComponent.class})
public interface DashboardComponent {
    void inject(DashboardFragment fragment);
}
