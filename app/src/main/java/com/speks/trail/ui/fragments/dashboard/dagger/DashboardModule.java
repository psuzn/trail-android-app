package com.speks.trail.ui.fragments.dashboard.dagger;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.ui.fragments.dashboard.mv.DashboardModel;
import com.speks.trail.ui.fragments.dashboard.mv.DashboardView;

import dagger.Module;
import dagger.Provides;

@Module
public
class DashboardModule {

    AppCompatActivity activity;

    public DashboardModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @DashboardScope
    DashboardModel model(APINetwork appNetwork, PreferencesManager preferencesManager) {
        return new DashboardModel(appNetwork, preferencesManager);
    }

    @Provides
    @DashboardScope
    DashboardView view() {
        return new DashboardView(activity);
    }

}
