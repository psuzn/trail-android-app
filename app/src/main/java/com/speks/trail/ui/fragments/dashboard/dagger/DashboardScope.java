package com.speks.trail.ui.fragments.dashboard.dagger;

import javax.inject.Scope;

@Scope
@interface DashboardScope {
}
