package com.speks.trail.ui.fragments.dashboard.mv;

import com.speks.trail.helpers.BaseModel;
import com.speks.trail.helpers.Constants;
import com.speks.trail.network.APINetwork;
import com.speks.trail.network.models.dashboard.DashboardResponse;
import com.speks.trail.storage.PreferencesManager;

import java.util.HashMap;

import io.reactivex.Observable;

public class DashboardModel extends BaseModel {
    public DashboardModel(APINetwork apiNetwork, PreferencesManager preferencesManager) {
        super(apiNetwork, preferencesManager);
    }

    public Observable<DashboardResponse> getDashboardDisposable(HashMap<String, String> params) {
        return getApiNetwork().dashboard(Constants.API_FAKE.BASE_URL + Constants.API_FAKE.DASHBOARD, params);
    }
}
