package com.speks.trail.ui.fragments.dashboard.mv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.speks.trail.R;
import com.speks.trail.databinding.ActivityHomeBinding;
import com.speks.trail.databinding.FragmentDashboardBinding;
import com.speks.trail.helpers.BaseView;

public class DashboardView extends BaseView {

    public FragmentDashboardBinding binding;
    AppCompatActivity activity;

    public DashboardView(AppCompatActivity activity) {
        super(activity);
        this.activity = activity;
        binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.fragment_dashboard, this, true);

    }

    @Override
    protected AppCompatActivity getActivity() {
        return activity;
    }
}
