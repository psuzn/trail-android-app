package com.speks.trail.ui.fragments.history.dagger;



import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.ui.fragments.history.HistoryFragment;

import dagger.Component;

@HistoryScope
@Component(modules = HistoryModule.class, dependencies = {AppComponent.class})
public interface HistoryComponent {
    void inject(HistoryFragment fragment);
}
