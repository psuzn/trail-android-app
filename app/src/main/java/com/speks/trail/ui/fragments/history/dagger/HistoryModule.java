package com.speks.trail.ui.fragments.history.dagger;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.ui.adaptor.ListAdaptor;
import com.speks.trail.ui.fragments.history.mv.HistoryModel;
import com.speks.trail.ui.fragments.history.mv.HistoryView;
import com.speks.trail.ui.fragments.recent.mv.RecentModel;
import com.speks.trail.ui.fragments.recent.mv.RecentView;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

@Module
public
class HistoryModule {

    AppCompatActivity activity;

    public HistoryModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @HistoryScope
    HistoryModel model(APINetwork appNetwork, PreferencesManager preferencesManager) {
        return new HistoryModel(appNetwork, preferencesManager);
    }

    @Provides
    @HistoryScope
    HistoryView view() {
        return new HistoryView(activity);
    }

    @Provides
    @HistoryScope
    ListAdaptor listAdaptor(Picasso picasso) {
        return new ListAdaptor(activity, picasso);
    }

}
