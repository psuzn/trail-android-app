package com.speks.trail.ui.fragments.history.mv;

import com.speks.trail.helpers.BaseModel;
import com.speks.trail.helpers.Constants;
import com.speks.trail.network.APINetwork;
import com.speks.trail.network.models.recent.RecentResponse;
import com.speks.trail.storage.PreferencesManager;

import java.util.HashMap;

import io.reactivex.Observable;

public class HistoryModel extends BaseModel {
    public HistoryModel(APINetwork apiNetwork, PreferencesManager preferencesManager) {
        super(apiNetwork, preferencesManager);
    }

    public Observable<RecentResponse> getRecentDisposable(HashMap<String, String> params) {
        return getApiNetwork().recent(Constants.API_FAKE.BASE_URL + Constants.API_FAKE.RECENT, params);
    }
}
