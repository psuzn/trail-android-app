package com.speks.trail.ui.fragments.recent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.speks.trail.app.Trail;
import com.speks.trail.helpers.Constants;
import com.speks.trail.network.models.recent.RecentResponse;
import com.speks.trail.ui.adaptor.ListAdaptor;
import com.speks.trail.ui.fragments.recent.dagger.DaggerRecentComponent;
import com.speks.trail.ui.fragments.recent.dagger.RecentModule;
import com.speks.trail.ui.fragments.recent.mv.RecentModel;
import com.speks.trail.ui.fragments.recent.mv.RecentView;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class RecentFragment extends Fragment {

    @Inject
    RecentModel model;
    @Inject
    RecentView view;
    @Inject
    ListAdaptor adaptor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DaggerRecentComponent.builder()
                .appComponent(Trail.getAppComponent((AppCompatActivity) getActivity()))
                .recentModule(new RecentModule((AppCompatActivity) getActivity()))
                .build()
                .inject(this);
        getData(false);
        view.binding.srRefreshLayout.setOnRefreshListener(() -> getData(true));
        view.binding.rvEvents.setAdapter(adaptor);
        return view;
    }


    private void getData(boolean refresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", model.getPreference(Constants.PKEYS.ACCESS_TOKEN));
        if (!refresh) view.showLoading();
        DisposableObserver<RecentResponse> disposableObserver =
                new DisposableObserver<RecentResponse>() {
                    @Override
                    public void onNext(RecentResponse response) {
                        view.hideLoading();
                        view.binding.srRefreshLayout.setRefreshing(false);
                        adaptor.setData(response.getEvents());
                        if (refresh) view.showToast("Refreshed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideLoading();
                        view.binding.srRefreshLayout.setRefreshing(false);
                        view.showNetworkError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                };
        model.getRecentDisposable(params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(disposableObserver);
        compositeDisposable.add(disposableObserver);
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}