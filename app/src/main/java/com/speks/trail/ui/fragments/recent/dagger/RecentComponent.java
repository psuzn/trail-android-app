package com.speks.trail.ui.fragments.recent.dagger;


import com.speks.trail.app.dagger.AppComponent;
import com.speks.trail.ui.fragments.recent.RecentFragment;

import dagger.Component;

@RecentScope
@Component(modules = RecentModule.class, dependencies = {AppComponent.class})
public interface RecentComponent {
    void inject(RecentFragment fragment);
}
