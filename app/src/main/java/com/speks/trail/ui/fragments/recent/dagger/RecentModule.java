package com.speks.trail.ui.fragments.recent.dagger;

import androidx.appcompat.app.AppCompatActivity;

import com.speks.trail.network.APINetwork;
import com.speks.trail.storage.PreferencesManager;
import com.speks.trail.ui.adaptor.ListAdaptor;
import com.speks.trail.ui.fragments.recent.mv.RecentModel;
import com.speks.trail.ui.fragments.recent.mv.RecentView;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

@Module
public
class RecentModule {

    AppCompatActivity activity;

    public RecentModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @RecentScope
    RecentModel model(APINetwork appNetwork, PreferencesManager preferencesManager) {
        return new RecentModel(appNetwork, preferencesManager);
    }

    @Provides
    @RecentScope
    RecentView view() {
        return new RecentView(activity);
    }

    @Provides
    @RecentScope
    ListAdaptor listAdaptor(Picasso picasso) {
        return new ListAdaptor(activity, picasso);
    }

}
