package com.speks.trail.ui.fragments.recent.dagger;

import javax.inject.Scope;

@Scope
@interface RecentScope {
}
