package com.speks.trail.ui.fragments.recent.mv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.speks.trail.R;
import com.speks.trail.databinding.FragmentRecentBinding;
import com.speks.trail.helpers.BaseView;

public class RecentView extends BaseView {

    public FragmentRecentBinding binding;
    AppCompatActivity activity;

    public RecentView(AppCompatActivity activity) {
        super(activity);
        this.activity = activity;
        binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.fragment_recent, this, true);

    }

    @Override
    protected AppCompatActivity getActivity() {
        return activity;
    }
}
