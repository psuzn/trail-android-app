package com.speks.trail.ui.views;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.speks.trail.R;
import com.speks.trail.databinding.ImagePreviewDialogBinding;
import com.speks.trail.network.models.recent.EventItem;
import com.speks.trail.ui.adaptor.ViewPagerAdaptor;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ImagePreviewDialog extends Dialog implements ViewPager.OnPageChangeListener, View.OnClickListener {
    private final EventItem eventItem;
    private int focusedItemIndex;
    private AppCompatActivity activity;
    private Picasso picasso;
    private ImagePreviewDialogBinding binding;

    public ImagePreviewDialog(@NonNull AppCompatActivity activity, EventItem eventItem,
                              int focusTo, Picasso picasso) {
        super(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        this.eventItem = eventItem;
        this.focusedItemIndex = focusTo;
        this.activity = activity;
        this.picasso = picasso;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.image_preview_dialog,
                null, false);

        setContentView(binding.getRoot());
        setCancelable(true);
        binding.backButton.setOnClickListener((v) -> dismiss());
        binding.viewPager.setAdapter(new ViewPagerAdaptor(activity, picasso, eventItem,this));
        binding.viewPager.addOnPageChangeListener(this);
        setupPageIndicator();
        binding.viewPager.post(() -> {
            binding.viewPager.setCurrentItem(focusedItemIndex);
            changePageIndicatorFocusedItemTo(focusedItemIndex);
        });

        binding.tvCctv.setText(eventItem.getCctv());
        binding.tvDate.setText(SimpleDateFormat.getDateTimeInstance().format(new Date(eventItem.getTime())));
    }

    private void setupPageIndicator() {
        LayoutInflater l = LayoutInflater.from(activity);
        for (String photo : eventItem.getPhotos()) {
            ImageView v = (ImageView) l.inflate(R.layout.dialog_page_indicator_item, binding.pageIndicator, false);
            picasso.load(photo).fit().centerCrop().placeholder(android.R.color.transparent).into(v, null);
            binding.pageIndicator.addView(v);
            v.setOnClickListener(v1 -> {
                int index = binding.pageIndicator.indexOfChild(v1);
                changePageIndicatorFocusedItemTo(index);
                binding.viewPager.setCurrentItem(index, true);
            });
        }
    }

    private void scrollPageIndicatorTo(int index) {
        int screenWidth = activity.getWindowManager()
                .getDefaultDisplay().getWidth();
        View view = binding.pageIndicator.getChildAt(index);
        int scrollX = (view.getLeft() - (screenWidth / 2)) + (view.getWidth() / 2);
        binding.horizontalScrollView.smoothScrollTo(scrollX, 0);
    }

    @SuppressWarnings("deprecation")
    private void changePageIndicatorFocusedItemTo(int index) {
        if (binding.pageIndicator.getChildCount() > index) {
            View oFocused = binding.pageIndicator.getChildAt(focusedItemIndex);
            oFocused.setPadding(0, 0, 0, 0);
            oFocused.setBackgroundColor(Color.argb(0, 0, 0, 0));
            oFocused.invalidate();
        }
        View nFocused = binding.pageIndicator.getChildAt(index);
        nFocused.setPadding(4, 4, 4, 4);
        nFocused.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));

        nFocused.invalidate();
        focusedItemIndex = index;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        changePageIndicatorFocusedItemTo(position);
        scrollPageIndicatorTo(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override //onClickOnViewPager
    public void onClick(View v) {
        int newVisibility = binding.pageIndicator.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE;
        binding.backButton.setVisibility(newVisibility);
        binding.pageIndicator.setVisibility(newVisibility);
        binding.tvCctv.setVisibility(newVisibility);
        binding.tvDate.setVisibility(newVisibility);
        binding.tvLicence.setVisibility(newVisibility);
    }
}